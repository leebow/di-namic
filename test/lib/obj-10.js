
module.exports = TestObj10;

function TestObj10(dependency8) {
    this.__counter = 0;
    this.__dependency8 = dependency8;
}

TestObj10.prototype.testMethod = function (count, callback) {
    this.__counter += count;
    callback(null, this.__counter);
};

TestObj10.prototype.testMethod2 = function (callback) {

    // dependency8.plantTree returns a promise
    this.__dependency8.plantTree()
        .then((result) => {
            callback(null, result);
        })
        .catch((err) => {
            callback(err);
        });
};

