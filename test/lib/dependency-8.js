module.exports = class Dependency8 {

    constructor() {
        this.__treeCount = 0;
    }

    async plantTree() {
        this.__treeCount += 1;
        return this.__treeCount;
    };

    getTreeCount() {
        return this.__treeCount;
    }
}