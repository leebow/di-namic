
module.exports = class TestObj11 {

    constructor(dependency8) {
        this.__counter = 0;
        this.__dependency8 = dependency8;
    }

    testMethod(count) {
        this.__counter += count;
        return this.__counter;
    };

    async testMethod2() {
        // dependency8.plantTree returns a promise
        try {
            return await this.__dependency8.plantTree()
        } catch (err) {
            console.log(err);
            throw err;
        }s
    };
}


