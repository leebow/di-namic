var expect = require('expect.js');

var Container = require('../../lib/containerV2');
var Obj1 = require('../lib/obj-1');
var Obj2 = require('../lib/obj-2');
var Obj3 = require('../lib/obj-3');
var Obj4 = require('../lib/obj-4');
var Obj5 = require('../lib/obj-5');
var Obj6 = require('../lib/obj-6');
var Obj7 = require('../lib/obj-7');
var Obj8 = require('../lib/obj-8');
var Obj9 = require('../lib/obj-9');
var Obj10 = require('../lib/obj-10');
var Obj11 = require('../lib/obj-11');
var Dependency1 = require('../lib/dependency-1');
var Dependency2 = require('../lib/dependency-2');
var Dependency3 = require('../lib/dependency-3');
var Dependency4 = require('../lib/dependency-4');
var Dependency5 = require('../lib/dependency-5');
var Dependency6 = require('../lib/dependency-6');
var Dependency7 = require('../lib/dependency-7');
var Dependency8 = require('../lib/dependency-8');

describe('unit - container v2', function () {

    this.timeout(30000);

    context('', function () {

        beforeEach('setup', function (done) {

            done();
        });

        afterEach('stop', function (done) {
            done();
        });

        it('successfully throws an error on register when no binding alias is present', async function () {

            var container = Container.getInstance();

            try {
                await container.register(null, { test: 'blah' });
                throw new Error('Error expected!')
            } catch (e) {
                expect(e.message).to.equal('binding alias is null or empty!');
            }
        });

        it('successfully throws an error on register when no binding dependency is present', async function () {

            var container = Container.getInstance();

            try {
                await container.register('Blah', null);
                throw new Error('Error expected!')
            } catch (e) {
                expect(e.message).to.equal('binding dependency is null or empty!');
            }
        });

        it('successfully throws an error on registerSingleton when no binding alias is present', async function () {

            var container = Container.getInstance();

            try {
                await container.registerSingleton(null, { test: 'blah' });
                throw new Error('Error expected!')
            } catch (e) {
                expect(e.message).to.equal('binding alias is null or empty!');
            }
        });

        it('successfully throws an error on registerSingleton when no binding dependency is present', async function () {

            var container = Container.getInstance();

            try {
                await container.registerSingleton('Blah', null);
                throw new Error('Error expected!')
            } catch (e) {
                expect(e.message).to.equal('binding dependency is null or empty!');
            }
        });

        it('successfully throws an error on registerFactory when no binding alias is present', async function () {

            var container = Container.getInstance();

            try {
                await container.registerFactory(null, { test: 'blah' }, 'yadda');
                throw new Error('Error expected!')
            } catch (e) {
                expect(e.message).to.equal('binding alias is null or empty!');
            }
        });

        it('successfully throws an error on registerFactory when no binding dependency is present', async function () {

            var container = Container.getInstance();

            try {
                await container.registerFactory('Blah', null, 'yadda');
                throw new Error('Error expected!')
            } catch (e) {
                expect(e.message).to.equal('binding dependency is null or empty!');
            }
        });

        it('successfully throws an error on registerFactory when no factory method is specified', async function () {

            var container = Container.getInstance();

            try {
                await container.registerFactory('Blah', { test: 'blah' }, null);
                throw new Error('Error expected!')
            } catch (e) {
                expect(e.message).to.equal('factory method must be specified!');
            }
        });

        it('successfully throws an error on registerSingletonFactory when no binding alias is present', async function () {

            var container = Container.getInstance();

            try {
                await container.registerSingletonFactory(null, { test: 'blah' }, 'yadda');
                throw new Error('Error expected!')
            } catch (e) {
                expect(e.message).to.equal('binding alias is null or empty!');
            }
        });

        it('successfully throws an error on registerSingletonFactory when no binding dependency is present', async function () {

            var container = Container.getInstance();

            try {
                await container.registerSingletonFactory('Blah', null, 'yadda');
                throw new Error('Error expected!')
            } catch (e) {
                expect(e.message).to.equal('binding dependency is null or empty!');
            }
        });

        it('successfully throws an error on registerSingletonFactory when no factory method is specified', async function () {

            var container = Container.getInstance();

            try {
                await container.registerSingletonFactory('Blah', { test: 'blah' }, null);
                throw new Error('Error expected!')
            } catch (e) {
                expect(e.message).to.equal('factory method must be specified!');
            }
        });

        it('successfully throws an error when no alias is specified on resolve', async function () {

            var container = Container.getInstance();

            try {
                await container.registerSingletonFactory('Blah', { test: 'blah' }, 'yadda');
                await container.resolve();
                throw new Error('Error expected!')
            } catch (e) {
                expect(e.message).to.equal('binding alias is null or empty!');
            }
        });

        it('successfully resolves first-level dependencies', async function () {

            var container = Container.getInstance();

            await container.register('Bob', Dependency1);
            await container.register('Mary', Dependency2);
            await container.register('Joe', Obj1, ['Bob', 'Mary']);

            // 'Joe' depends on 'Bob' and 'Mary'
            var result = await container.resolve('Joe');

            return new Promise((resolve, reject) => {
                result.testMethod(function (err, testResult) {
                    console.log(testResult);
                    if (testResult == 'success')
                        return resolve();

                    return reject(new Error('Unexpected result: ' + testResult));
                });
            });

        });

        it('successfully registers and resolves when no constructor dependencies required', async function () {

            var container = new Container();

            await container.register('Joe', Obj6);

            var result = await container.resolve('Joe');

            return new Promise((resolve, reject) => {
                result.testMethod(function (err, testResult) {

                    console.log(testResult);

                    if (testResult === 'success')
                        return resolve();

                    return reject(new Error('Unexpected result: ' + testResult));
                });
            });
        });

        it('successfully resolves nested dependencies', async function () {

            var container = new Container();

            await container.register('Bob', Dependency1);
            // 'Jack' depends on 'Bob'
            await container.register('Jack', Dependency4, ['Bob']);
            // 'Joe' depends on 'Jack'
            await container.register('Jim', Obj4, ['Jack']);

            var result = await container.resolve('Jim');

            return new Promise((resolve, reject) => {
                result.testMethod(function (err, testResult) {

                    console.log(testResult);

                    if (testResult === 'success')
                        return resolve();

                    return reject(new Error('Unexpected result: ' + testResult));
                });
            });
        });

        it('successfully resolves anonymous object value dependencies', async function () {

            /*
             ie: a constructor dependency is an anonymous object, which itself contains a
             key whose value is an object that we've registered...

             MyPrototype(arg1, {key1: '@inject:dependency_x'})
             */

            var container = new Container();

            await container.register('Bob', Dependency1);
            // anonymous object 'Ellie' depends on 'Bob'
            await container.register('Ellie', { key1: '@inject:Bob', key2: 'Blah' });
            // 'Chuck' depends on 'Ellie'
            await container.register('Chuck', Obj5, ['Ellie']);

            var result = await container.resolve('Chuck');

            return new Promise((resolve, reject) => {
                result.testMethod(function (err, testResult) {

                    console.log(testResult);

                    if (testResult === 'success')
                        return resolve();

                    return reject(new Error('Unexpected result: ' + testResult));
                });
            });

        });

        it('successfully resolves anonymous object directly', async function () {

            var container = new Container();

            await container.register('Freddie', { key1: 'Hello' });

            var result = await container.resolve('Freddie');

            expect(result.key1).to.equal('Hello');
        });

        it('successfully resolves a static object as a dependency', async function () {

            var container = new Container();

            await container.register('Milo', Dependency3);
            // 'Mask' depends on 'Milo'
            await container.register('Mask', Obj2, ['Milo']);

            var result = await container.resolve('Mask');
            var testResult = result.testMethod();

            expect(testResult).to.equal('success');
        });

        it('successfully resolves a static object directly', async function () {

            var container = new Container();

            await container.register('Milo', Dependency3);

            var result = await container.resolve('Milo');

            expect(result.testMethod()).to.equal('success');
        });

        it('successfully resolves a static object directly 2', async function () {
            var container = new Container();

            await container.register('Toast', 'buttered');
            await container.register('Marmite', Dependency5, ['Toast']);

            var result = await container.resolve('Marmite');

            console.log('BINDINGS LENGTH: ', container.bindingLen);

            expect(result.testMethod()).to.equal('buttered');
        });

        it('successfully resolves a number primitive', async function () {
            var container = new Container();

            await container.register('Olive', 2);
            // 'Popeye' depends on 'Olive'
            await container.register('Popeye', Obj3, ['Olive']);

            var result = await container.resolve('Popeye');

            var testResult = result.testMethod();

            expect(testResult).to.equal(2);
        });

        it('successfully resolves a boolean primitive', async function () {
            var container = new Container();

            await container.register('Olive', true);
            // 'Popeye' depends on 'Olive'
            await container.register('Popeye', Obj3, ['Olive']);

            var result = await container.resolve('Popeye');

            var testResult = result.testMethod();

            expect(testResult).to.equal(true);
        });

        it('successfully resolves a string primitive', async function () {
            var container = new Container();

            await container.register('Olive', 'Squeak!');
            // 'Popeye' depends on 'Olive'
            await container.register('Popeye', Obj3, ['Olive']);

            var result = await container.resolve('Popeye');

            var testResult = result.testMethod();

            expect(testResult).to.equal('Squeak!');
        });

        it('successfully resolves an object instance from a factory', async function () {

            var container = Container.getInstance();

            await container.registerFactory('Gin', Dependency6, 'create');
            await container.register('Tonic', Obj7, ['Gin']);

            var result = await container.resolve('Tonic');

            return new Promise((resolve, reject) => {
                result.testMethod(function (err, testResult) {
                    console.log(testResult);
                    if (testResult == 'Slurp!')
                        return resolve();

                    return reject(new Error('Unexpected result: ' + testResult));
                });
            });
        });

        it('successfully resolves a singleton object instance', async function () {

            var container = Container.getInstance();

            await container.registerSingleton('Mega', Obj8);

            var mega = await container.resolve('Mega');

            return new Promise((resolve, reject) => {

                mega.testMethod(1, function (err, result) {

                    // expect(result).to.equal(1);
                    console.log(result);

                    if (result != 1)
                        return reject(new Error('Unexpected result: ' + result));

                    container.resolve('Mega')
                        .then((mega2) => {

                            mega2.testMethod(5, function (err, result) {

                                console.log(result);

                                if (result != 6)
                                    return reject(new Error('Unexpected result: ' + result));

                                resolve();
                            });
                        })
                        .catch((err) => {
                            reject(err);
                        });
                });
            });
        });

        it('successfully resolves a singleton object dependency', async function () {

            var container = Container.getInstance();

            await container.registerSingleton('Mighty', Dependency7);
            await container.register('Mouse', Obj8, ['Mighty']);

            var mouse = await container.resolve('Mouse');

            return new Promise((resolve, reject) => {

                mouse.testMethod2(function (err, result) {

                    console.log(result);

                    if (result != 1)
                        return reject(new Error('Unexpected result: ' + result));

                    container.resolve('Mouse')
                        .then((mouse2) => {

                            mouse2.testMethod2(function (err, result) {

                                console.log(result);

                                if (result != 2)
                                    return reject(new Error('Unexpected result: ' + result));

                                container.resolve('Mighty')
                                    .then((mighty) => {

                                        let treeCount = mighty.getTreeCount();

                                        console.log(treeCount);

                                        if (treeCount == 2)
                                            return resolve();

                                        return reject(new Error('Unexpected result: ' + treeCount));
                                    })
                                    .catch((err) => {
                                        reject(err);
                                    });
                            });
                        })
                        .catch((err) => {
                            reject(err);
                        });
                });
            });

        });

        it('successfully resolves a singleton ES6 object dependency', async function () {

            var container = Container.getInstance();

            await container.registerSingleton('John', Dependency8);
            await container.register('Deere', Obj10, ['John']);

            var singleton = await container.resolve('John');
            var deere = await container.resolve('Deere');

            console.log('Tree count: ' + singleton.getTreeCount());

            return new Promise((resolve, reject) => {

                deere.testMethod2(function (err, result) {

                    let treeCount = singleton.getTreeCount();
                    console.log('Tree count: ' + treeCount);

                    if (treeCount != result)
                        return reject(new Error('Unexpected result: ' + treeCount));

                    // resolve the singleton again
                    container.resolve('John')
                        .then((result) => {

                            let treeCount2 = result.getTreeCount();
                            console.log('Tree count: ' + treeCount2);

                            if (treeCount != treeCount2)
                                return reject(new Error('Unexpected result: ' + treeCount2));

                            resolve();
                        })
                        .catch((err) => {
                            reject(err);
                        });
                });
            })

        });

        it('successfully resolves a singleton factory object dependency', async function () {

            var container = Container.getInstance();

            await container.registerSingletonFactory('Donald', Obj9, 'create');

            var donald = await container.resolve('Donald');

            return new Promise((resolve, reject) => {

                donald.testMethod(5, function (err, result) {

                    console.log(result);

                    if (result != 5)
                        return reject(new Error('Unexpected result: ' + result));

                    container.resolve('Donald')
                        .then((donald2) => {
                            donald2.testMethod(11, function (err, result) {

                                console.log(result);

                                if (result != 16)
                                    return reject(new Error('Unexpected result: ' + result));

                                resolve();
                            });
                        })
                        .catch((err) => {
                            reject(err);
                        });
                });
            });
        });
    });

    it('successfully resolves a singleton class-based object dependency', async function () {

        var container = Container.getInstance();

        await container.registerSingleton('Caterpillar', Dependency8);
        await container.register('Digger', Obj11, ['Caterpillar']);

        var digger = await container.resolve('Digger');

        // resolve the singleton
        var singleton = await container.resolve('Caterpillar');

        // get the initial tree count
        console.log('Tree count: ' + singleton.getTreeCount());

        // invoke the test method
        let result = await digger.testMethod2();

        // get the tree count again
        let treeCount = singleton.getTreeCount();
        console.log('Tree count: ' + treeCount);

        if (treeCount != result)
            return reject(new Error('Unexpected result: ' + treeCount));

        // resolve the singleton again
        let result2 = await container.resolve('Caterpillar');
        let treeCount2 = result2.getTreeCount();
        console.log('Tree count: ' + treeCount2);

        if (treeCount != treeCount2)
            throw new Error('Unexpected result: ' + treeCount2);

    });
});
